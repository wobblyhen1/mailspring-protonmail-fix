# mailspring-protonmail-fix

Despite being arguably the best mail client for Linux, <a href="https://www.getmailspring.com/"><b>Mailspring</b></a> is not yet fully compatible for security- and privacy-focused email providers such as <a href="https://proton.me/"><b>Proton Mail</b></a>.
In particular, users have been reporting(1) about a ceaseless messages 'loop' within ProtonMail accounts in Mailspring: emails appear, then disappear, then diseappear again...

## Workaround 
This is a simple workaround (with ProtonMail v. 13.3.3, Mailspring Mail Pro) which draws on previous answers gathered on the web to solve <i>sync issues between Mailspring and Proton Mail</i><sup>(1)</sup>:

1) The problem: Mails are present on the client until a new sync cycle. 
2) Inspection in /Home/.config/mailsync*.log (the most recent) reveals some errors:<br><i>
23671 [2024-03-17 23:05:14.314] [background] [error] Could not create Mailspring container folder: Mailspring. ErrorCreate
23671 [2024-03-17 23:05:14.314] [background] [error] Could not create required Mailspring folder: Mailspring/Snoozed. ErrorCreate</i><br>
Apparently, ProtonMail forbids Mailspring to create a first-level folder (Mailspring) and a nester folder (Mailspring/Snoozed) which Mailspring needs for its functioning <sup>(2)</sup>
3) But the ProtonMail interface comes in handy as it allows us to create just the folders we need - a top-level 'Mailspring' folder, and a nested 'Snoozed' one:<br>
![proton.png](protonmail.png)
4) Finally, we have to instruct Mailspring to use our new container for its operation: Preferences, Update Connection Settings on your ProtonMail account, set Mailspring/Folders as a Custom Container Folder. <br>
![mailspring.png](mailspring.png)
<br>
Now we can connect just fine and the ProtonMail messages will not be as squishy as they were (or at least, they are not for me!)


(1) https://community.getmailspring.com/t/protonmail-problem-with-mailsync-process/5208 
https://community.getmailspring.com/t/protonmail-all-emails-disappear-from-inbox-and-reappear-then-disappear-again/309
https://www.reddit.com/r/ProtonMail/comments/g7svtf/mailspring_silently_fails_to_work_with_protonmail/
https://community.getmailspring.com/t/cannot-sync-126-163-mails-because-setting-up-snooze-subfolder-in-mail-folders-is-not-allowed/562/5
https://community.getmailspring.com/t/disappearing-emails-desync/428/28

(2) https://community.getmailspring.com/t/remove-mailspring-snoozed/677
